import { useEffect, useState } from 'react';
import { Formik, FormikProps, Form } from 'formik';
import DatePicker from 'react-datepicker';

import { IInitialValues } from './types/form.types';
import { InputField, InputFieldAsyncValidation } from './components/InputField';
import Spinner from './components/assets/spinner.gif';
import { MySelect } from './components/Select/Select';
import { Report } from './components/Report';
import { SuccessfulSubmit } from './components/SuccessfulSubmit/SuccessfulSubmit';
import { validationSchema } from './validation/validationSchema';
import { creditFormApi, initialSelectorsData } from '../../api/submitForm';

import 'react-datepicker/dist/react-datepicker.css';
import './index.css';

const initialFormData: IInitialValues = {
  email: '',
  activityType: '',
  dateCompleted: null,
  CPDType: '',
  CPDAmount: '',
  description: '',
  reflection: '',
};

const maxDate = new Date();
maxDate.setDate(maxDate.getDate() - 1);

export const CreditForm: React.FC = () => {
  const [selectData, setSelectData] = useState(initialSelectorsData);
  const [isValidEmail, setIsValidEmail] = useState(false);
  const [formSubmited, setFormSubmited] = useState(false);

  useEffect(() => {
    creditFormApi.getDateForCreditForm()
      .then(setSelectData);
  }, []);

  if (formSubmited) {
    return <SuccessfulSubmit />;
  }

  return (
    <div className="flex items-center">
      <Formik
        initialValues={initialFormData}
        validationSchema={validationSchema}
        onSubmit={async (values, { setSubmitting, resetForm }) => {
          try {
            await creditFormApi.submitForm(values);
            setFormSubmited(true);
            setTimeout(() => setFormSubmited(false), 2000);
          } catch (error) {
            alert(error || "Something went wrong");
          }
          setSubmitting(false);
          setIsValidEmail(false)
          resetForm();
        }}
      >
        {({ values, setFieldValue, isSubmitting, isValid }: FormikProps<IInitialValues>) => (
          <Form className="flex flex-col md:flex-row w-full rounded-lg">
            <div className="flex flex-col justify-between w-full md:w-1/2 py-10 md:py-20 px-8 sm:px-20 md:px-14 md:py-20 bg-[#fff] overflow-hidden">
              <div className="">
                <h2 className="text-2xl md:text-3xl lg:text-4xl font-extrabold text-center mb-3">
                  Zing365
                </h2>
                <p className="text-center mb-3 font-medium">Enter your CPD credit details below</p>
                <InputFieldAsyncValidation
                  name="email"
                  placeholder="Email"
                  checkIcon={true}
                  isFieldValid={isValidEmail}
                  setIsFieldValid={setIsValidEmail}
                />
                <MySelect
                  options={selectData.activityType}
                  name="activityType"
                  placeholder="Select an Activity Type"
                  setFieldValue={setFieldValue}
                />
                <DatePicker
                  selected={values.dateCompleted}
                  disabled={isSubmitting}
                  onKeyDown={e => e.preventDefault()}
                  onChange={(date: Date | null) => setFieldValue('dateCompleted', date)}
                  maxDate={maxDate}
                  className="w-full py-3 px-4 bg-[#eeeeee] placeholder:text-slate-400 placeholder:font-medium mb-20"
                  placeholderText="Date Completed"
                />
              </div>
              <Report email={values.email} disabled={!isValidEmail} />
            </div>
            <div className="w-full md:w-1/2 py-10 md:py-20 px-8 sm:px-20 md:px-14 bg-[#66869b]">
              <div className="mb-3">
                <MySelect
                  options={selectData.CPDType}
                  name="CPDType"
                  placeholder="Select a Credit Type"
                  setFieldValue={setFieldValue}
                />
                <MySelect
                  options={selectData.CPDAmount}
                  name="CPDAmount"
                  placeholder="Select Credit Amount"
                  setFieldValue={setFieldValue}
                />
                <InputField name="description" placeholder="Description" />
                <InputField name="reflection" placeholder="Reflection" />
              </div>
              <p className="text-[#dae5ea] text-[17px] text-center mb-5">
                Ensure all details are correct before submission
              </p>
              <button
                disabled={!isValidEmail || isSubmitting || !isValid}
                type="submit"
                className="relative m-auto block bg-[#0090a8] py-3 px-11 rounded-full text-[#dae5ea] font-medium disabled:opacity-40"
              >
                SUBMIT CREDIT
                {isSubmitting && <img src={Spinner} className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2" />}
              </button>
            </div>
          </Form>
        )}
      </Formik>
    </div>
  );
};
