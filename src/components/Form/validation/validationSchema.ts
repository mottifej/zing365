import * as Yup from 'yup';

export const validationSchema = Yup.object().shape({
  activityType: Yup.string().required('Required'),
  dateCompleted: Yup.string().required('Required'),
  CPDType: Yup.string().required('Required'),
  CPDAmount: Yup.string().required('Required'),
  description: Yup.string().max(1000, 'Max length 1000 symbols'),
  reflection: Yup.string().max(1000, 'Max length 1000 symbols'),
});

export const validationEmailSchema = Yup.string().required('Required').email('Invalid email format');
