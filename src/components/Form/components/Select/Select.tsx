import { FC, memo } from 'react';
import Select, { SingleValue } from 'react-select';

type TOption = { value: string; label: string };

interface SelectProps {
  name: string;
  options: string[];
  placeholder?: string;
  setFieldValue: (field: string, value: string, shouldValidate?: boolean | undefined) => void;
}

export const MySelect: FC<SelectProps> = memo(({ options, placeholder, name, setFieldValue }) => {
  const onSelect = (newValue: SingleValue<TOption>) => {
    setFieldValue(name, newValue?.value || '');
  };

  return (
    <Select
      className="mb-5"
      options={options.map(value => ({ value, label: value }))}
      placeholder={placeholder}
      isSearchable={false}
      onChange={onSelect}
      styles={{
        control: baseStyles => ({
          ...baseStyles,
          backgroundColor: '#eeeeee',
          padding: '6px',
          border: 'none',
          boxShadow: 'none',
          borderRadius: 'unset',
        }),
        placeholder: baseStyles => ({
          ...baseStyles,
          fontWeight: 'bold',
          color: '#212121',
        }),
        indicatorSeparator: () => ({
          display: 'none',
        }),
      }}
    />
  );
});
