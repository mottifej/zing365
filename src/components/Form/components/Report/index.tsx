
import DatePicker from 'react-datepicker';
import { useState } from 'react';
import cn from 'classnames';
import Spinner from '../assets/spinner.gif';
import { creditFormApi } from '../../../../api/submitForm';
import { InputFileField } from '../InputField';
import ArrowIcon from './assets/arrow.svg?react';

interface IReportdProps {
  disabled: boolean;
  email: string;
}

export const Report: React.FC<IReportdProps> = ({ disabled, email }) => {
  const dateTo = new Date();
  const dateFrom = new Date(new Date().setDate(dateTo.getDate() - 7));

  const [reportRequest, setReportRequest] = useState(false);
  const [range, setRange] = useState({ dateFrom, dateTo });
  const [visible, setVisible] = useState(false);

  const getReport = () => {
    setReportRequest(true);
    creditFormApi
      .getReport(email, range)
      .catch(e => alert(e.message || 'Server Error'))
      .finally(() => setReportRequest(false));
  }
  return (
  <div className="relative">
    <div className="absolute t-0 -left-1/2 w-[200%] border-t border-gray-200"/>
    <div className="w-full mt-5">
      <button
        type="button"
        onClick={() => setVisible(!visible)}
        className="flex w-full flex-col items-center justify-center gap-x-1.5 rounded-md bg-white px-3 pt-3 text-sm font-semibold text-gray-900 shadow-sm bg-gray-100 hover:bg-gray-200"
        >
        <p>Reports</p>
        <ArrowIcon className={cn('text-gray-400 fill-current', { 'rotate-180': visible })} />
      </button>
    </div>
    {visible && (
      <div className="pt-3">
        <div className="py-1" role="none">
        <div className="flex">
          <DatePicker
            selected={range.dateFrom}
            disabled={disabled}
            maxDate={range.dateTo}
            onKeyDown={e => e.preventDefault()}
            dateFormat="dd-MM-yyyy"
            onChange={dateFrom => dateFrom && setRange({ ...range, dateFrom })}
            className="w-full mr-0.5 py-3 px-4 bg-[#eeeeee] placeholder:text-slate-400 placeholder:font-medium mb-5 disabled:opacity-40"
            placeholderText="Date Completed"
          />
          <DatePicker
            selected={range.dateTo}
            disabled={disabled}
            minDate={range.dateFrom}
            maxDate={dateTo}
            onKeyDown={e => e.preventDefault()}
            dateFormat="dd-MM-yyyy"
            onChange={dateTo => dateTo && setRange({ ...range, dateTo })}
            className="w-full ml-0.5 py-3 px-4 bg-[#eeeeee] placeholder:text-slate-400 placeholder:font-medium mb-5 disabled:opacity-40"
            placeholderText="Date Completed"
          />
        </div>
        <button
          disabled={disabled || reportRequest || !range.dateFrom || !range.dateTo }
          onClick={getReport}
          className="relative m-auto block bg-[#0090a8] py-3 px-11 rounded-full text-[#dae5ea] font-medium disabled:opacity-40 mb-5"
        >
          Download report
          {reportRequest && <img src={Spinner} className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2" />}
        </button>
        <InputFileField
            name="Import_report"
            labelname="Upload report"
            fileSubmit={file => creditFormApi.sendReport(file)}
          />
        </div>
      </div>
    )}
  </div>
  );
};
