import { InputHTMLAttributes, useState } from 'react';
import cn from 'classnames';
import CompleteIcon from './assets/complete.svg?react';
import Spinner from '../assets/spinner.gif';

interface IInputFileProps extends InputHTMLAttributes<HTMLInputElement> {
  name: string;
  labelname: string;
  styles?: string;
  checkIcon?: boolean;
  fileSubmit: (file: File) => Promise<void>;
}

export const InputFileField: React.FC<IInputFileProps> = ({
  styles,
  fileSubmit,
  labelname,
  disabled,
  ...props
}) => {
  const [fileSubmiting, setFileSubmiting] = useState(false);
  const [fileSubmited, setFileSubmited] = useState(false);
  const [fileSubmitResp, setFileSubmitResp] = useState('');
  const [fileSubmitError, setFileSubmitError] = useState('');

  const fileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const  [file] = e.currentTarget.files || [];
    setFileSubmiting(true);
    setFileSubmitError('');
    setFileSubmitResp('');
    setFileSubmited(false);
    fileSubmit(file)
      .then(data => {setFileSubmitResp(JSON.stringify(data))})
      .catch((e: Error) => setFileSubmitError(e.message))
      .finally(() => {
        setFileSubmiting(false);
        setFileSubmited(true);
      });
  }

  return (
    <div className="relative mb-5">
      <label htmlFor="small-file-input" className="block mb-2 text-[17px] text-center dark:text-white">{labelname || "Upload your file"}</label>
      <input
        className={cn(
          "block w-full border border-gray-200 shadow-sm rounded-lg text-sm focus:z-10",
          "focus:border-blue-500 focus:ring-blue-500",
          "disabled:opacity-50 disabled:pointer-events-none",
          "dark:bg-[#eeeeee] dark:border-gray-700 dark:text-gray-400 dark:focus:outline-none dark:focus:ring-1 dark:focus:ring-gray-600",
          "file:bg-[#0090a8] file:border-0 file:bg-gray-100 file:me-4 file:py-2 file:px-3",
          "dark:file:bg-gray-700 dark:file:text-gray-400",
          styles
        )}
        type="file"
        id="small-file-input"
        accept=".csv"
        disabled={disabled || fileSubmiting}
        onChange={fileChange}
        {...props}
      />
      {fileSubmitError && <div className="text-red-400">{fileSubmitError}</div>}
      {fileSubmitResp && <div className="text-blue-400 text-center">{fileSubmitResp}</div>}
      {fileSubmiting && <img src={Spinner} className="absolute top-1/2 right-[-30px]" />}
      {fileSubmited && !fileSubmitError && (
        <CompleteIcon className="absolute top-1/2 right-[-30px]" />
      )}
    </div>
  );
};
