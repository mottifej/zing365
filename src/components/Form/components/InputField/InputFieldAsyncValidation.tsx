import { InputHTMLAttributes, useState, useCallback } from 'react';
import { useField } from 'formik';
import cn from 'classnames';
import debounce from 'lodash.debounce';
import CompleteIcon from './assets/complete.svg?react';
import Spinner from '../assets/spinner.gif';
import { creditFormApi } from '../../../../api/submitForm';
import { validationEmailSchema } from '../../validation/validationSchema';

interface IInputFieldProps extends InputHTMLAttributes<HTMLInputElement> {
  name: string;
  styles?: string;
  checkIcon?: boolean;
  isFieldValid: boolean;
  setIsFieldValid: (isValid: boolean) => void;
}

export const InputFieldAsyncValidation: React.FC<IInputFieldProps> = ({
  name,
  styles,
  checkIcon,
  isFieldValid,
  setIsFieldValid,
  ...props
}) => {
  const [validationError, setValidationError] = useState('');
  const [isValidating, setIsValidating] = useState(false);
  const [field] = useField(name);

  const validateEmail = useCallback(debounce((value: string) => {
    setIsValidating(true);
    creditFormApi.validateEmail(value)
      .then(res => {
        setIsValidating(false);
        setValidationError(res);
        setIsFieldValid(!res);
    })}, 500), []);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setIsFieldValid(false);
    setIsValidating(false);
    setValidationError('');
    validationEmailSchema
      .validate(e.currentTarget.value)
      .then(validateEmail, () => {});

    field.onChange(e);
  };

  const handleBlure = (e: React.FocusEvent<HTMLInputElement>) => {
    validationEmailSchema
      .validate(e.currentTarget.value)
      .catch(({ message }: Error) => setValidationError(message));
  };

  return (
    <div className="relative mb-5">
      <input
        className={cn(
          'bg-[#eeeeee] pl-4 pr-9 py-3 placeholder:text-slate-400 placeholder:font-medium w-full focus:outline-0',
          { 'outline outline-1 outline-red-400': Boolean(validationError) },
          styles
        )}
        {...field}
        onChange={handleChange}
        onBlur={handleBlure}
        {...props}
      />
      {validationError && <div className="text-red-400">{validationError}</div>}
      {isValidating && <img src={Spinner} className="absolute right-2 top-6 -translate-y-1/2" />}
      {checkIcon && isFieldValid && !isValidating && (
        <CompleteIcon className="absolute right-2 top-6 -translate-y-1/2" />
      )}
    </div>
  );
};
