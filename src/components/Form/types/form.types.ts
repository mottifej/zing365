export interface IInitialValues {
  email: string;
  activityType: string;
  dateCompleted: Date | null;
  CPDType: string;
  CPDAmount: string;
  description: string;
  reflection: string;
}

export interface ICreditFormData {
  activityType: string[];
  CPDType: string[];
  CPDAmount: string[];
}
