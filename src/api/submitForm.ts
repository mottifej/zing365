import { ICreditFormData, IInitialValues } from '../components/Form/types/form.types';

export const initialSelectorsData: ICreditFormData = {
  activityType: [],
  CPDType: [],
  CPDAmount: [],
};

class CreditFormApi {
  constructor(private apiUrl: string) {}

  async submitForm(values: Required<IInitialValues>) {
    const parsedDate = new Date(values.dateCompleted!).toISOString().slice(0, 10);

    try {
      const resp = await fetch(`${this.apiUrl}/submit_form`, {
        body: JSON.stringify({ ...values, dateCompleted: parsedDate }),
        method: 'POST',
        headers: { 'Content-Type': 'application/json' }
      });

      if(!resp.ok) {
        const errorMessage = await resp.text();
        throw errorMessage;
      }
    } catch (error) {
      throw error || 'server error, check input data'
    }
  }

  async validateEmail(email: string): Promise<string> {
    try {
      const resp = await fetch(`${this.apiUrl}/validate_email`, {
        body: JSON.stringify({ email }),
        method: 'POST',
        headers: { 'Content-Type': 'application/json' }
      });

      if(!resp.ok) {
        throw resp;
      }

      return '';
    } catch (error) {
      return 'User with this email not found';
    }
  }

  async sendReport(file: File): Promise<void> {
    const formData = new FormData();
    formData.append('file', file);
    try {
      const resp = await fetch(`${this.apiUrl}/import`, {
        body: formData,
        method: 'POST',
      });

      if(!resp.ok) {
        throw 'server error';
      }
      return await resp.json();
    } catch (error) {
      if(typeof error === 'string') {
        throw new Error(error)
      }
      throw error;
    }
  }

  async getReport(email: string, { dateFrom, dateTo }: { dateFrom: Date, dateTo: Date }) {
    try {
      const resp = await fetch(`${this.apiUrl}/generate_report`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email,
          dateFrom: dateFrom.toISOString().slice(0, 10),
          dateTo: dateTo.toISOString().slice(0, 10)
        }),
      })

      if(!resp.ok) {
        const errorMessage = await resp.text();
        throw errorMessage;
      }
      const blob = await resp.blob();
      const aElement = document.createElement('a');
      aElement.setAttribute('download', `report_${new Date().toISOString().slice(0, 16).replace(':', '-')}.csv`);
      const href = URL.createObjectURL(blob);
      aElement.href = href;
      aElement.setAttribute('target', '_blank');
      aElement.click();
      URL.revokeObjectURL(href);

    } catch (error) {
      if(typeof error === 'string') {
        throw new Error(error)
      }
      throw error;
    }
  }

  async getDateForCreditForm(): Promise<ICreditFormData> {
    try {
      const response = await fetch('form_data.json');
      return await response.json();
    } catch (error) {
      return initialSelectorsData;
    }
  }
}

export const creditFormApi = new CreditFormApi(import.meta.env.VITE_API_URL);
