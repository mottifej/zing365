- Live Demo [Form](https://zing365-mottifej-463dd4ad95a6c87d8c274c23fd14fa8dfc486d59356316.gitlab.io)
 
### development mod
- npm run dev
- open your brouser at _http://localhost:5173

### production mod
- change `api_url` in `.env` file
- run in your console - `npm run build`
- move all files from `public` directory to your static server